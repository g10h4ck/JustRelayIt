#!/bin/bash

LIBRESAPI="http://127.0.0.1:8888/api/v2"

cd "$( dirname "${BASH_SOURCE[0]}" )"
date > lastlog

# $1 path of file containing the node key
add_trusted_node()
{
	curl --silent \
		-H 'Content-Type: application/json' \
		-H 'Accept: application/json' \
		-X POST --data "{\"cert_string\":\"$(cat "${1}" | tr '\n' ' ' | sed 's/\s/\\n/g')\"}" \
		${LIBRESAPI}/peers
	(>&2 echo "add_trusted_node() $@")
}

# $1 PGP id of the node to remove from friends
remove_trusted_node()
{
	curl --silent ${LIBRESAPI}/peers/${1}/delete
	(>&2 echo "remove_trusted_node() $@")
}


# $1 path of file containing the node key
get_pgp_id_from_key()
{
	curl --silent \
		-H 'Content-Type: application/json' \
		-H 'Accept: application/json' \
		-X POST --data "{\"cert_string\":\"$(cat "${1}" | tr '\n' ' ' | sed 's/\s/\\n/g')\"}" \
		${LIBRESAPI}/peers/examine_cert \
		| jq '.data.pgp_id' | tr -d '"'
	(>&2 echo "get_pgp_id_from_key() $@")
}

# Update from GIT
git fetch --all &>> lastlog
git reset --hard origin/master &>> lastlog

declare -A ALL_FRIENDS

for kFile in trusted_nodes/* ; do
	add_trusted_node $kFile &>> lastlog
	ALL_FRIENDS[$(get_pgp_id_from_key $kFile 2>> lastlog)]=true
done

for tNode in $(curl --silent ${LIBRESAPI}/peers | jq '.[] | .[] | .pgp_id' | tr -d '"' | tr '\n' ' ')  ; do
	[ "${ALL_FRIENDS[$tNode]}" == "true" ] || remove_trusted_node $tNode &>> lastlog
done 2>>lastlog
